package View;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import Model.WordandNgram;

public class View {
	
	public ArrayList<String> Start(){
		WordandNgram n = new WordandNgram();
		 String ans1,ans2;
	        ans1 = JOptionPane.showInputDialog(null, "Input Text");
	        System.out.println(ans1);
	        ans2 = JOptionPane.showInputDialog(null, "Input ngram");
	        int ans = Integer.parseInt(ans2);
	        n.generateNgram(ans1,ans);
	        JOptionPane.showMessageDialog(null,"Answer: "+n.getNgram());
	        System.exit(0);
			return n.getNgram();
	}
	public static void main(String[] args) {
		View test = new View();
		test.Start();
	}
}
